package cn.edu.zucc.wuh.dao;

import cn.edu.zucc.wuh.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserMapper {
    int deleteByPrimaryKey(String host);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String host);

    User editPwd(String host, String pwd);

    List<User> queryUserByHostAndPassword(String host, String pwd);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> getUserAll();
}