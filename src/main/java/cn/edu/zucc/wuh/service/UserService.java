package cn.edu.zucc.wuh.service;

import cn.edu.zucc.wuh.dao.UserMapper;
import cn.edu.zucc.wuh.entity.User;
import cn.edu.zucc.wuh.form.UserEditForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2020-06-18 12:18:17
 */
public interface UserService {

    /**
     * 通过ID查询单条数据
     *
     * @param host 主键
     * @return 实例对象
     */
    Boolean isUserExist(String host);
    User selectByPrimaryKey(String host);
    List<User> getUserAll();
    User editUser(UserEditForm user);
    List<User> queryUserByHostAndPassword(String host, String pwd);
    User editPwd(String host, String pwd);
    User login(String host, String pwd);
    User queryById(String host);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<User> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User update(User user);

    /**
     * 通过主键删除数据
     *
     * @param host 主键
     * @return 是否成功
     */
    boolean deleteById(String host);

}