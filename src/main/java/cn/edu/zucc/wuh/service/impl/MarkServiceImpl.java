package cn.edu.zucc.wuh.service.impl;

import cn.edu.zucc.wuh.dao.MarkMapper;
import cn.edu.zucc.wuh.entity.Mark;
import cn.edu.zucc.wuh.service.MarkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Mark)表服务实现类
 *
 * @author makejava
 * @since 2020-06-18 12:18:10
 */
@Service("markService")
public class MarkServiceImpl implements MarkService {
    @Resource
    private MarkMapper markMapper;
    /**
     * 通过ID查询单条数据
     *
     * @param mid 主键
     * @return 实例对象
     */
    @Override
    public Mark queryById(String mid) {
        return this.markMapper.selectByPrimaryKey(mid);
    }

    /**
     * 查询多条数据
     *
     * @return 对象列表
     */
    @Override
    public List<Mark> queryAllByLimit() {
        return this.markMapper.getMarkAll();
    }

    /**
     * 新增数据
     *
     * @param mark 实例对象
     * @return 实例对象
     */
    @Override
    public Mark insert(Mark mark) {
        this.markMapper.insert(mark);
        return mark;
    }

    /**
     * 修改数据
     *
     * @param mark 实例对象
     * @return 实例对象
     */
    @Override
    public Mark update(Mark mark) {
        this.markMapper.updateByPrimaryKey(mark);
        return this.queryById(mark.getMid());
    }

    /**
     * 通过主键删除数据
     *
     * @param mid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String mid) {
        return this.markMapper.deleteByPrimaryKey(mid) > 0;
    }

    @Override
    public List<Mark> getMarkByUser(String host) {
        return markMapper.getMarkByUser(host);
    }

    @Override
    public List<Mark> getMarkAll() {
        return markMapper.getMarkAll();
    }

    @Override
    public Mark getMarkByPidAndHost(String pid, String host) {
        return markMapper.selectByPidAndHost(pid,host);
    }
}