package cn.edu.zucc.wuh.service;

import cn.edu.zucc.wuh.entity.Mark;
import java.util.List;

/**
 * (Mark)表服务接口
 *
 * @author makejava
 * @since 2020-06-18 12:18:09
 */
public interface MarkService {

    /**
     * 通过ID查询单条数据
     *
     * @param mid 主键
     * @return 实例对象
     */
    Mark queryById(String mid);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Mark> queryAllByLimit();

    /**
     * 新增数据
     *
     * @param mark 实例对象
     * @return 实例对象
     */
    Mark insert(Mark mark);

    /**
     * 修改数据
     *
     * @param mark 实例对象
     * @return 实例对象
     */
    Mark update(Mark mark);

    /**
     * 通过主键删除数据
     *
     * @param mid 主键
     * @return 是否成功
     */
    boolean deleteById(String mid);

    List<Mark> getMarkByUser(String host);
    List<Mark> getMarkAll();
    Mark getMarkByPidAndHost(String pid,String host);
}