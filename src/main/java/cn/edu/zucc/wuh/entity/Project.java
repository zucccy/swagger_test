package cn.edu.zucc.wuh.entity;

public class Project {
    private String pid;

    private String videoUrl;

    private String picUrl;

    private String codeUrl;

    private Integer sampleNum;

    private Double sampleProportion;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl == null ? null : videoUrl.trim();
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

    public String getCodeUrl() {
        return codeUrl;
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl == null ? null : codeUrl.trim();
    }

    public Integer getSampleNum() {
        return sampleNum;
    }

    public void setSampleNum(Integer sampleNum) {
        this.sampleNum = sampleNum;
    }

    public Double getSampleProportion() {
        return sampleProportion;
    }

    public void setSampleProportion(Double sampleProportion) {
        this.sampleProportion = sampleProportion;
    }
}