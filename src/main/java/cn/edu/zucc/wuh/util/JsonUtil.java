package cn.edu.zucc.wuh.util;
import cn.edu.zucc.wuh.form.MarkForm;
import cn.edu.zucc.wuh.form.MarkedForm;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.Page;
import cn.edu.zucc.wuh.entity.*;
import java.util.ArrayList;
import java.util.List;
public class JsonUtil {
    public JSONObject user2JSON(User user){
        JSONObject tmp = new JSONObject();
        tmp.put("host", user.getHost());
        tmp.put("name", user.getName());

        JSONObject ret = new JSONObject();
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);
        return ret;
    }
    public JSONObject userInfo2Json(User user){
        JSONObject tmp = new JSONObject();
        tmp.put("host", user.getHost());
        tmp.put("name", user.getName());
        tmp.put("email", user.getEmail());
        tmp.put("phone", user.getPhone());
        tmp.put("school",user.getSchool());
        tmp.put("title",user.getTitle());
        tmp.put("course",user.getCourse());
        tmp.put("degree",user.getDegree());
        tmp.put("year_of_teaching",user.getYearOfTeaching());
        JSONObject ret = new JSONObject();
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);

        return ret;
    }
    public JSONObject ProjectPage2Json(Page<Project> page){
        JSONObject ret = new JSONObject();
        JSONObject tmp = new JSONObject();
        tmp.put("page_total", page.getTotalPages());
        tmp.put("page_num", page.getNumber()+1);
        tmp.put("page_size", page.getSize());
        List<Project> projects = page.getContent();
        tmp.put("projects", projects);
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);

        return ret;
    }
    public JSONObject MarkedProjectPage2Json(Page<MarkedForm> page){
        JSONObject ret = new JSONObject();
        JSONObject tmp = new JSONObject();
        tmp.put("page_total", page.getTotalPages());
        tmp.put("page_num", page.getNumber()+1);
        tmp.put("page_size", page.getSize());
        List<MarkedForm> projects = page.getContent();
        tmp.put("projects", projects);
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);

        return ret;
    }
    public JSONObject Project2Json(Project project){
        JSONObject tmp = new JSONObject();
        tmp.put("pid", project.getPid());
        tmp.put("video_url", project.getVideoUrl());
        tmp.put("pic_url", project.getPicUrl());
        tmp.put("code_url",project.getCodeUrl());
        tmp.put("sample_num",project.getSampleNum());
        tmp.put("sample_proportion",project.getSampleProportion());
        JSONObject ret = new JSONObject();
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);
        return ret;

    }
    public JSONObject Marked2Json(MarkedForm markedForm){
        JSONObject tmp = new JSONObject();
        tmp.put("pid", markedForm.getPid());
        tmp.put("video_url", markedForm.getVideoUrl());
        tmp.put("pic_url", markedForm.getPicUrl());
        tmp.put("code_url",markedForm.getCodeUrl());
        tmp.put("sample_num",markedForm.getSampleNum());
        tmp.put("sample_proportion",markedForm.getSampleProportion());
        tmp.put("score", markedForm.getScore());
        JSONObject ret = new JSONObject();
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);
        return ret;

    }
    public JSONObject Mark2Json(Mark mark){
        JSONObject tmp=new JSONObject();
        tmp.put("mid",mark.getMid());
        tmp.put("pid",mark.getPid());
        tmp.put("createUser",mark.getCreateUser());
        tmp.put("score",mark.getScore());
        tmp.put("createTime",mark.getCreateTime());
        tmp.put("lastModifiedTime",mark.getLastModifiedTime());
        tmp.put("status",mark.getStatus());
        JSONObject ret = new JSONObject();
        ret.put("code", Statue.SUCCESS);
        ret.put("data", tmp);
        return ret;
    }
}
