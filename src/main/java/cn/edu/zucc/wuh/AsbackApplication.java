package cn.edu.zucc.wuh;

import cn.edu.zucc.wuh.util.SpringUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
@SpringBootApplication
@MapperScan("cn.edu.zucc.wuh.dao")

@Import(SpringUtil.class)
public class AsbackApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AsbackApplication.class, args);
    }

    @Override//为了打包springboot项目
    protected SpringApplicationBuilder configure(
            SpringApplicationBuilder builder) {
        return builder.sources(this.getClass());
    }
}
